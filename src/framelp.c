#include "framelp.h"
#include "w_wadmgr.h"
#include "g_world.h"

extern uint16_t *sweet16;

extern void pixblt_binary_to_linear(uint16_t **saddr, 
									uint32_t sptch, 
									void *daddr,
									uint32_t dptch,
									PACKED_XY dydx,
									uint32_t color0,
									uint32_t color1);

extern void pixblt_binary_to_xy(uint16_t **saddr, 
								uint32_t sptch, 
								PACKED_XY daddr,
								uint32_t dptch,
								PACKED_XY dydx,
								uint32_t color0,
								uint32_t color1);


struct FGState fg_state;
struct R_RaycasterState *raycasterState;

extern uint8_t worldmap[24][24];

FIXED zbuffer[240];

/*
void FrameRoundRect(struct FGState *state, struct Rect *r, int16_t ovalW, int16_t ovalH)
{
	struct XY tl, tr, bl, br;

	struct XY oval_center;
	struct Rect oval_boundary;

    SetXY(&tl, r->x + (ovalW/2), r->y);
    SetXY(&tr, r->x + r->w - (ovalW/2), r->y);
    SetXY(&bl, r->x + (ovalW/2), r->y + r->h);
    SetXY(&br, r->x + r->w - (ovalW/2), r->y + r->h);

    LineFromTo(state, &tl, &tr);
    LineFromTo(state, &bl, &br);

    SetXY(&tl, r->x, r->y + (ovalH/2));
    SetXY(&tr, r->x + r->w, r->y + (ovalH/2));
    SetXY(&bl, r->x, r->y + r->h - (ovalH/2));
    SetXY(&br, r->x + r->w, r->y + r->h - (ovalH/2));

	LineFromTo(state, &tl, &bl);
    LineFromTo(state, &tr, &br);

	SetXY(&oval_center, r->x + (ovalW/2), r->y + (ovalH/2));
	SetRect(&oval_boundary, r->x, r->y, ovalW, ovalH);
	OvalPointCalculations(state, &oval_boundary, &oval_center, NULL, FILL_NO, QUADRANT_II);

	SetXY(&oval_center, r->x + r->w - (ovalW/2), r->y + (ovalH/2));
	SetRect(&oval_boundary, r->x + r->w, r->y, ovalW, ovalH);
	OvalPointCalculations(state, &oval_boundary, &oval_center, NULL, FILL_NO, QUADRANT_I);

	SetXY(&oval_center, r->x + (ovalW/2), r->y + r->h - (ovalH/2));
	SetRect(&oval_boundary, r->x, r->y + r->h, ovalW, ovalH);
	OvalPointCalculations(state, &oval_boundary, &oval_center, NULL, FILL_NO, QUADRANT_III);

	SetXY(&oval_center, r->x + r->w - (ovalW/2), r->y + r->h - (ovalH/2));
	SetRect(&oval_boundary, r->x + r->w, r->y + r->h, ovalW, ovalH);
	OvalPointCalculations(state, &oval_boundary, &oval_center, NULL, FILL_NO, QUADRANT_IV);

}

enum QUADRANT DegreesToQuadrant(int16_t degrees)
{
	degrees = abs(degrees) % 360;

	if(degrees >= 0 && degrees < 90)
	{
		return QUADRANT_I;
	}
	else if(degrees >= 90 && degrees < 180)
	{
		return QUADRANT_II;
	}
	else if(degrees >= 180 && degrees < 270)
	{
		return QUADRANT_III;
	}
	else
	{
		return QUADRANT_IV;
	}
}

void FrameArc(struct FGState *state, struct Rect *r, int16_t degreesStart, int16_t degreesEnd)
{
	struct XY midpoint_horiz_l, midpoint_horiz_r;
	struct XY midpoint_vert_u, midpoint_vert_d;
	struct XY center;
	char dbg[100];

	PACKED_XY packed_p;

	struct CLIP_COORDINATES clip_area;

	enum QUADRANT quad_start = DegreesToQuadrant(degreesStart);

	clip_area.upper_bound = 0;
	clip_area.lower_bound = 239;		

	SetXY(&center, r->x + (r->w >> 1), r->y + (r->h >> 1));
	SetXY(&midpoint_horiz_l, r->x, r->y + (r->h / 2));
	SetXY(&midpoint_horiz_r, r->x + r->w, r->y + (r->h / 2));
	SetXY(&midpoint_vert_u, r->x + (r->w / 2), r->y);
	SetXY(&midpoint_vert_d, r->x + (r->w / 2), r->y + r->h);

	clip_area.left_bound = floor(cos(degToRad(degreesStart)) * midpoint_horiz_r.x) + 1;
	clip_area.right_bound = floor(cos(degToRad(degreesEnd)) * midpoint_horiz_r.x) + 1;

	if(clip_area.left_bound > clip_area.right_bound)
	{
		int16_t temp;
		temp = clip_area.left_bound;
		clip_area.left_bound = clip_area.right_bound;
		clip_area.right_bound = temp;
	}

	PackXY(&packed_p, &midpoint_horiz_l);
	plot_pixel(state->pen_state.pattern, packed_p);
	PackXY(&packed_p, &midpoint_horiz_r);
	plot_pixel(state->pen_state.pattern, packed_p);
	PackXY(&packed_p, &midpoint_vert_u);
	plot_pixel(state->pen_state.pattern, packed_p);
	PackXY(&packed_p, &midpoint_vert_d);
	plot_pixel(state->pen_state.pattern, packed_p);

	OvalPointCalculations(state, r, &center, &clip_area, FILL_NO, QUADRANT_ALL);
}
*/

time_t frameTime, oldFrameTime;				/* used for timing */

struct Texture tex;

/* Initialize the raycaster's frame loop. */
void FrameStart()
{
	printf("FrameStart\n");

	TEX_LoadAllTexturesFromWad(texture_lookup, MAX_TEXTURES);

	raycasterState = malloc(sizeof(struct R_RaycasterState));

	InitFGState(&fg_state);
	ResetPitchAndWindow();

	printf("Initializing player state\n");
	raycasterState->player_position.x = FX_FROMFLOAT(12.0); raycasterState->player_position.y = FX_FROMFLOAT(12.0);
	raycasterState->player_direction.x = FX_FROMFLOAT(-1.0); raycasterState->player_direction.y = FX_FROMFLOAT(0.0);
	raycasterState->camera_plane.x = FX_FROMFLOAT(0.0); raycasterState->camera_plane.y = FX_FROMFLOAT(0.66);

	/* set up front and back buffers */
	printf("Setting up front and back buffers\n");
	fg_state.front_buffer = (void *)0;
	fg_state.front_DPYSTRT = 0xFFFC;
	fg_state.back_buffer = (void *)0x100000;
	fg_state.back_DPYSTRT = 0xEFFC;

	raycasterState->zbuffer = zbuffer;

	R_CalculateRotationSpeeds(&rotationTable, 0.125f);

	FrameLoop();
}

void RC_RedrawBG()
{
	struct Rect r;

	WaitForDIFlag();
	SetRect(&r, 41, 41, 240, 79);
	PenPat(&fg_state, 0x0000);
	FillRect(&fg_state, &r);

	SetRect(&r, 41, 121, 240, 79);
	PenPat(&fg_state, 0x0606);
	FillRect(&fg_state, &r);

	SetRect(&r, 40, 40, 320-40-40+1, 240-80);
	fg_state.pen_state.pattern = 0xFFFF;
	FrameRect(&fg_state, &r);
}

void FrameLoop()
{
	int ray_x;

	const uint16_t VISIBLE_HEIGHT = SCREEN_HEIGHT - 80;
	const uint16_t VISIBLE_WIDTH = SCREEN_WIDTH - 80;
	const uint16_t HALF_VISIBLE_HEIGHT = SCREEN_HEIGHT/2;
	const uint16_t HALF_VISIBLE_WIDTH = SCREEN_WIDTH/2;

	const FIXED FX_VISIBLE_HEIGHT = FX_FROMINT(SCREEN_HEIGHT - 41);
	const FIXED FX_VISIBLE_WIDTH = FX_FROMINT(SCREEN_WIDTH - 80);
	const FIXED FX_HALF_VISIBLE_HEIGHT = FX_FROMINT(SCREEN_HEIGHT >> 1);
	const FIXED FX_HALF_VISIBLE_WIDTH = FX_FROMINT(SCREEN_HEIGHT >> 1);

	/* Line segment */
	struct XY from, to;
	uint16_t color;
	FIXED drawStart, drawEnd;

	struct Rect r;

	const int leftmost_ray = 0;
	const int fx_leftmost_ray = FX_FROMINT(fx_leftmost_ray);

	FIXED forward_vector_x, forward_vector_y;

	FIXED wall_intercept_x;
	uint8_t texel_x;
	FIXED texel_y_step;
	FIXED texture_position;

	uint8_t wall_height;

	FIXED sprite_pos_x, sprite_pos_y;

	struct XY tex_dest;

	FIXED sprite_distances[16];

	FIXED wall_frac_x;
	FIXED wall_texel_x;

	int texture_id;

	sprite_pos_x = FX_FROMFLOAT(22.0);
	sprite_pos_y = FX_FROMFLOAT(12.0);

	G_LoadMap("MAP01");

	while(1)
	{
		SwapBuffers(&fg_state);
		PutDebugString("Kate's Labyrinth", 40, 220, 0xFFFF);

		RC_RedrawBG();
	
		I_MovePlayer(raycasterState, &rotationTable, worldmap);

		for(ray_x=leftmost_ray; ray_x<SCREEN_WIDTH-80; ray_x += 2)
		{
			raycasterState->camera.x = FX_DIV(FX_FROMINT(ray_x << 1), FX_FROMINT(VISIBLE_WIDTH)) - 0x00010000; /* X coordinate in camera space */
			raycasterState->camera.y = raycasterState->camera.x;

			/* We only want the integer parts of these values. */
			raycasterState->map_position.x = raycasterState->player_position.x & 0xFFFF0000; 	/* map coordinate, X */
			raycasterState->map_position.y = raycasterState->player_position.y & 0xFFFF0000;	/* map coordinate, Y */

			/* length of ray from one X or Y side to next X or Y side */
			raycasterState->ray_direction.x = FX_ADD(raycasterState->player_direction.x, FX_FastMUL(raycasterState->camera_plane.x, raycasterState->camera.x));
			raycasterState->ray_direction.y = FX_ADD(raycasterState->player_direction.y, FX_FastMUL(raycasterState->camera_plane.y, raycasterState->camera.y));
			raycasterState->delta_distance.x = (raycasterState->ray_direction.y == 0) ? 0 : ((raycasterState->ray_direction.x == 0) ? 1 : abs(FX_DIV(0x00010000, raycasterState->ray_direction.x)));
			raycasterState->delta_distance.y = (raycasterState->ray_direction.x == 0) ? 0 : ((raycasterState->ray_direction.y == 0) ? 1 : abs(FX_DIV(0x00010000, raycasterState->ray_direction.y)));

			/* Calculate step and initial sideDist. */
			if (raycasterState->ray_direction.x < 0)
			{
				raycasterState->step.x = 0xFFFF0000;
				raycasterState->side_distance.x = FX_FastMUL( FX_SUB(raycasterState->player_position.x, raycasterState->map_position.x), raycasterState->delta_distance.x );
			}
			else
			{
				raycasterState->step.x = 0x00010000;
				raycasterState->side_distance.x = FX_FastMUL( FX_SUB(raycasterState->map_position.x + 0x00010000, raycasterState->player_position.x), raycasterState->delta_distance.x );
			}
			if (raycasterState->ray_direction.y < 0)
			{
				raycasterState->step.y = 0xFFFF0000;
				raycasterState->side_distance.y = FX_FastMUL( FX_SUB(raycasterState->player_position.y, raycasterState->map_position.y), raycasterState->delta_distance.y );
			}
			else
			{
				raycasterState->step.y = 0x00010000;
				raycasterState->side_distance.y = FX_FastMUL( FX_SUB(raycasterState->map_position.y + 0x00010000, raycasterState->player_position.y), raycasterState->delta_distance.y );
			}

			/* Perform DDA. */
			R_DDA(raycasterState);

			/* Calculate distance projected on camera direction (Euclidean distance will give fisheye effect!) */
			if (raycasterState->side == 0) raycasterState->fx_perpWallDist = FX_DIV(raycasterState->map_position.x - raycasterState->player_position.x + FX_DIV(0x00010000 - raycasterState->step.x, 0x00020000), raycasterState->ray_direction.x);
			else		   				   raycasterState->fx_perpWallDist = FX_DIV(raycasterState->map_position.y - raycasterState->player_position.y + FX_DIV(0x00010000 - raycasterState->step.y, 0x00020000), raycasterState->ray_direction.y);

			/* Calculate height of line to draw on screen */
			raycasterState->fxLineHeight = FX_DIV(FX_VISIBLE_HEIGHT, raycasterState->fx_perpWallDist);

			/* calculate lowest and highest pixel to fill in current stripe */
			drawStart = -FX_DIV(raycasterState->fxLineHeight, 0x00020000) + FX_HALF_VISIBLE_HEIGHT;
			drawEnd = FX_DIV(raycasterState->fxLineHeight, 0x00020000) + FX_HALF_VISIBLE_HEIGHT;

			if(drawStart < 0x00290000) drawStart = 0x00290000;
			if(drawEnd >= FX_VISIBLE_HEIGHT+1) drawEnd = FX_VISIBLE_HEIGHT;

			/* Z Buffer for wall distances */
			/*
			raycasterState->zbuffer[ray_x] = raycasterState->fx_perpWallDist;
			raycasterState->zbuffer[ray_x+1] = raycasterState->fx_perpWallDist;
			*/

			texture_id = worldmap[FX_TOINT(raycasterState->map_position.y)][FX_TOINT(raycasterState->map_position.x)];

			if(raycasterState->side == 0) wall_frac_x = raycasterState->player_position.y + FX_FastMUL(raycasterState->fx_perpWallDist, raycasterState->ray_direction.y);
			else						  wall_frac_x = raycasterState->player_position.x + FX_FastMUL(raycasterState->fx_perpWallDist, raycasterState->ray_direction.x);
			wall_frac_x = wall_frac_x & 0x0000FFFF;	/* only care about the fractional coordinate */

			wall_texel_x = FX_FastMUL(wall_frac_x, 0x00400000);
			if(raycasterState->side == 0 && raycasterState->ray_direction.x > 0) wall_texel_x = 0x00400000 - wall_texel_x - 0x00010000;
			if(raycasterState->side == 1 && raycasterState->ray_direction.y < 0) wall_texel_x = 0x00400000 - wall_texel_x - 0x00010000;

			draw_textured_wall_slice(fg_state.back_buffer, texture_lookup[texture_id].data, FX_TOINT(wall_texel_x), texture_lookup[texture_id].size.y, ray_x+41, FX_TOINT(drawStart), FX_TOINT(drawEnd - drawStart));
			draw_textured_wall_slice(fg_state.back_buffer, texture_lookup[texture_id].data, FX_TOINT(wall_texel_x), texture_lookup[texture_id].size.y, ray_x+42, FX_TOINT(drawStart), FX_TOINT(drawEnd - drawStart));
		}
	}
}