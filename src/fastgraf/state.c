#include "state.h"
#include "pen.h"

#include "util/printf.h"
#include "v_video.h"

void InitFGState(struct FGState *state)
{
	printf("Init FGState\n");

    SetXY(&state->window_size, 320, 240);
    SetXY(&state->pen_state.location, 0, 0);
    PenNormal(state);

    state->front_buffer = (void *)0;
    state->back_buffer = (void *)0x100000;

    state->front_DPYSTRT = 0xFFFC;
    state->back_DPYSTRT = 0xEFFC;
}

void SwapBuffers(struct FGState *state)
{
	void *temp_bfr;
	uint16_t temp_dpystrt;

	temp_bfr = state->front_buffer;
	state->front_buffer = state->back_buffer;
	state->back_buffer = temp_bfr;

	temp_dpystrt = state->front_DPYSTRT;
	state->front_DPYSTRT = state->back_DPYSTRT;
	state->back_DPYSTRT = temp_dpystrt;

	MMIO16(DPYSTRT) = state->front_DPYSTRT;

	/*
    printf("Buffers swapped. backbfr is now %08X, frontbfr is now %08X. dpystrt is %04X\n", state->back_buffer, state->front_buffer, MMIO16(DPYSTRT));
	*/
}