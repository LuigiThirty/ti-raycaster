#ifndef PRIMS_DRAWFONT_H
#define PRIMS_DRAWFONT_H

#include <stdint.h>

#include "fastgraf/globals.h"
#include "fastgraf/types/xy.h"

void Put8x16FontChar(uint16_t **source, uint8_t c, int16_t destX, int16_t destY, uint16_t bg, uint16_t fg);
void PutDebugString(char *string, int16_t x, int16_t y, uint16_t color);

#endif