#ifndef LINE_H
#define LINE_H

#include <stdint.h>

#include "v_video.h"
#include "fastgraf/state.h"

/* High-level */
void LineTo(struct FGState *fg, int16_t x, int16_t y);
void LineToXY(struct FGState *fg, struct XY *to);

/* Low-level */
void LineFromTo(struct FGState *fg, struct XY *from, struct XY *to);

/* ASM primitive funcs */
void draw_line(void *backbuffer, uint32_t from, uint32_t to);

#endif