    .global _draw_line

    .text

    .include "include\tms34010.equ"

_draw_line:
    ; Trampoline for the ASM primitive.
    ; Fetch the start and end points from the stack frame.
    mmtm	SP,a0,a1,a2,FP     ; save SP, [any registers that will be modified], FP
	move	STK,FP          ; stack frame
    move	*FP(-32),a0,1   ; first param 
	move	*FP(-64),a1,1   ; second param
	move	*FP(-96),a2,1	; third param
	move	a0,b4			; start of the display buffer
    move    a1,b0           ; fn expects params in b0/b2 
    move    a2,b2
    calla   draw_line_prim  ; call the draw routine

	move    *SP(32*5),STK,1 ; restore caller STK
    mmfm	SP,a0,a1,a2,FP  ; restore SP, [any registers that will be modified], FP
	rets	2               ; fix stack

;;;;;;
; Draw a line from point (xs,ys) to point (xe,ye)
; B0 is (xs,ys) and B2 is (xe,ye) in [Y,X] format.
;
; From page 12-105 of the 34010 manual
draw_line_prim:
	clr		b10
	clr		b11
	clr		b12
	clr		b14
	movi	0FFFFFFFFh,b13
	subxy	b2,b0	; calculate A and B

; Now set up B7 = (a,b) and B11 = (dx_diag,dy_diag). Assume that
; A < 0 and B < 0. If A >= 0 or B >= 0, make corrections later.
;	B11 (INC1) contains dy_diag::dx_diag
;	B12 (INC2) contains dy_nondiag::dx_nondiag

	movi	-1,b11		; dx_diag = dy_diag - 1
	movk	1,b12		; Constant = 1
	clr		b7
	subxy	b0,b7		; B7 = (-a,-b)
	jrnc	L1			; Jump if B < 0

; Handle case where B >= 0:
	movy	b0,b7		; Make A in B7 positive.
	srl		15,b11		; change dy_diag to +1
L1:
	jrnv	L2			; Jump if A < 0

; Handle case where A >= 0:
	movx	b0,b7		; Take absolute value of A
	movx	b12,b12		; Change dx_diag to +1
L2:
	movx	b11,b12		; dx_nondiag = dx_diag, dy_nondiag=0

; Compare magnitudes of A and B
	move	b7,b0		; Copy A and B
	srl		16,b0		; Move B into 16 LSBs
	cmpxy	b0,b7		; Compare A and B
	jrnv	L3			; Jump if A >= B

; Handle case where A < B; must swap A and B so A >= B
	movx	b7,b0		; copy B into B0
	rl		16,b7		; swap A and B halves of B7
	clr		b12
	movy	b11,b12		; dx_nondiag=0, dy_nondiag=dy_diag

; Calculate initial values of decision variable (D) and loop counter:
L3:
	add		b0,b0		; B0 = 2*B
	movx	b7,b10		; B10 = A
	sub		b10,b0		; B0 = d(2*B-A)
	addk	1,b10		; Loop count = A+1 (in B10)

; Draw line and return to caller.
	LINE
	rets
;;;;;;;;;;