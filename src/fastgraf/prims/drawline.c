#include "fastgraf/prims/drawline.h"

void LineFromTo(struct FGState *fg, struct XY *from, struct XY *to)
{
	/* Draw a line between FROM and TO of color COLOR1.
	 * Uses the ASM _draw_line primitive.
	 * B0 <- from
	 * B2 <- to
	 */
	 
	PACKED_XY asm_from;	/* A9 */
	PACKED_XY asm_to;	/* A10 */
	
	/* Convert the XY values to packed coordinates. */
	PackXY(&asm_from, from);
	PackXY(&asm_to, to);

	asm_from = (from->y << 16) | from->x;
	asm_to = (to->y << 16) | to->x;
	
	/* Need to set the COLOR1 register to the line color. */
	Set_COLOR1(fg->pen_state.pattern);	

	draw_line(fg->back_buffer, asm_from, asm_to);
}

void LineTo(struct FGState *fg, int16_t x, int16_t y)
{
	struct XY p;
	SetXY(&p, x, y);

	/* Draw a line from fg->pen_position to the point TO with pattern fg->pen_pattern. */
	Set_COLOR1(fg->pen_state.pattern);

	LineFromTo(fg, &fg->pen_state.location, &p);
	MoveTo(fg, &p);
}

void LineToXY(struct FGState *fg, struct XY *to)
{
	/* Draw a line from fg->pen_position to the point TO with pattern fg->pen_pattern. */
	Set_COLOR1(fg->pen_state.pattern);

	LineFromTo(fg, &fg->pen_state.location, to);
	MoveTo(fg, to);
}
