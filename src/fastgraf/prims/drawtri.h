#ifndef FASTGRAF_PRIMS_DRAWTRI_H
#define FASTGRAF_PRIMS_DRAWTRI_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "fastgraf/state.h"
#include "fastgraf/prims/drawline.h"
#include "fastgraf/types/rect.h"
#include "fastgraf/types/xy.h"
#include "fastgraf/types/tri.h"

void FrameTriangle(struct FGState *state, struct Triangle *t);

#endif