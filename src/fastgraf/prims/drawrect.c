#include "fastgraf/prims/drawrect.h"
#include "fastgraf/prims/drawline.h"

void FrameRect(struct FGState *state, struct Rect *r)
{
    /* Frame a Rect by drawing 4 lines. */

    struct XY tl, tr, bl, br;

    SetXY(&tl, r->x, r->y);
    SetXY(&tr, r->x + r->w, r->y);
    SetXY(&bl, r->x, r->y + r->h);
    SetXY(&br, r->x + r->w, r->y + r->h);

    LineFromTo(state, &tl, &tr);
    LineFromTo(state, &tl, &bl);
    LineFromTo(state, &tr, &br);
    LineFromTo(state, &bl, &br);
}

void FillRect(struct FGState *state, struct Rect *r)
{
    PACKED_XY origin, size;
    struct XY p;

    SetXY(&p, r->x, r->y);
    PackXY(&origin, &p);
    SetXY(&p, r->w, r->h);
    PackXY(&size, &p);

    prim_fill_rect(state->back_buffer, origin, size, state->pen_state.pattern);
}