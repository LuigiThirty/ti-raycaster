    .text
    .include "include\tms34010.equ"

    .global _plot_pixel
_plot_pixel:
	; Plot a pixel in VRAM.
	; Input:
	;	- A0 <- BYTE 	color
	;	- A1 <- XY 		address to plot

    mmtm	SP,a0,a1,FP     ; save SP, [any registers that will be modified], FP
	move	STK,FP          ; stack frame
    
    move    *FP(-32),a0,1   ; param 1   - BYTE color
    move	*FP(-64),a1,1   ; param 2   - PACKED_XY coord
	pixt	a0,*a1.xy		; Dereference the XY coord in A1. Write the pixel value A0 to it.
    
    move    *SP(32*4),STK,1         ; restore caller STK
    mmfm	SP,a0,a1,FP     ; save SP, [any registers that will be modified], FP
	rets    2

_FX_DIV_reg:
    ; a8 = a1 / a3

    ; Need to shift the dividend left by 16 bits into a0.
    ; It then becomes a0:a1 / a3.
    move    a1,a0
    srl     16,a0                   ; 0000AAAA:AAAABBBB
    sll     16,a1                   ; 0000AAAA:BBBB0000

    ; Sign-extend A0 if required.
    btst    15,a0
    jreq    __fx_div_calc ; Z=0 if bit is 1
__fx_div_sext:
    ori     0FFFF0000h,a0

__fx_div_calc:
    divs    a3,a0                   ; Rd / Rs, so 0000AAAA:BBBB0000 / CCCCDDDD
    move    a0,a8

    rets
;;;;;;;;;;;;;;

; todo: move this to a new file
    .global _copy_image
_copy_image:
    mmtm	SP,a0,a1,a2,a3,a4,a5,FP   ; save SP, [any registers that will be modified], FP
	move	STK,FP              ; stack frame
    
    move    *FP(-32*1),a0,1     ; param 1   - ptr destination framebuffer (320x240)
    move    *FP(-32*2),a1,1     ; param 2   - ptr source address (SADDR)
    move	*FP(-32*3),a2,1     ; param 3   - uint16_t source X size
    move    *FP(-32*4),a3,1     ; param 4   - uint16_t source Y size
    move    *FP(-32*5),a4,1     ; param 5   - int16_t destination X
    move    *FP(-32*6),a5,1     ; param 6   - int16_t destination Y

    move    a0,OFFSET           ; copy OFFSET
    move    a1,SADDR            ; copy SADDR
    move    a2,SPTCH
    sll     3,SPTCH

    sll     16,a5
    or      a5,a4               ; pack the destination coordinate
    move    a4,DADDR            ; DADDR is the destination coordinate

    sll     16,a3
    or      a3,a2
    move    a2,DYDX

    ; populate CONVSP with source pitch
    move    SPTCH,a2
    lmo     a2,a2
    move    a2,@CONVSP

    ; dest pitch should be the same, always
    pixblt  l,xy
        
    move    *SP(32*8),STK,1     ; restore caller STK
    mmfm	SP,a0,a1,a2,a3,a4,a5,FP   ; restore SP, [any registers that will be modified], FP
	rets    2

;;;;;;;;;;;;;;;;;;;;;;;
    .global _copy_image_scaled
_copy_image_scaled:
    mmtm	SP,a0,a1,a2,a3,a4,a5,a6,a7,a9,a10,a11,FP   ; save SP, [any registers that will be modified], FP
	move	STK,FP              ; stack frame
    
    move    *FP(-32*1),a0,1     ; param 1   - ptr destination framebuffer (320x240)
    move    *FP(-32*2),a1,1     ; param 2   - ptr source address (SADDR)
    move	*FP(-32*3),a2,1     ; param 3   - uint16_t source X size
    move    *FP(-32*4),a3,1     ; param 4   - uint16_t source Y size
    move    *FP(-32*5),a4,1     ; param 5   - int16_t destination X
    move    *FP(-32*6),a5,1     ; param 6   - int16_t destination Y
    move    *FP(-32*7),a6,1     ; param 7   - uint16_t destination X size
    move    *FP(-32*8),a7,1     ; param 8   - uint16_t destination Y size

    move    a0,OFFSET           ; copy OFFSET
    move    a1,SADDR            ; copy SADDR
    move    a2,SPTCH

    ; source X size / dest X size = texel X step
    move	*FP(-32*3),a1,1     ; param 3   - uint16_t source X size
    move    *FP(-32*7),a3,1     ; param 7   - uint16_t destination X size
    calla   _FX_DIV_reg
    move    a8,b10  ; B10 = texel X step

    move    *FP(-32*4),a1,1     ; param 4   - uint16_t source Y size
    move    *FP(-32*8),a3,1     ; param 8   - uint16_t destination Y size
    calla   _FX_DIV_reg
    move    a8,b11  ; B11 = texel Y step

    clr     b12   ; current texel X
    clr     b13   ; current texel Y

    move    a2,b14  ; B14 = size X
    sll     16,b14  ; make size X a fixed point number

    move    *FP(-32*5),a4,1     ; param 5   - int16_t destination X
    move    *FP(-32*6),a5,1     ; param 6   - int16_t destination Y
    sll     16,a5
    or      a5,a4   ; pack destination coordinate into A4

    setf    8,0,0
    move    *FP(-32*2),a9,1     ; param 2   - ptr source address (SADDR)
    move    *FP(-32*4),a10,1    ; param 4   - uint16_t source Y size
    move    *FP(-32*5),a11,1     ; param 5   - int16_t destination X

    move    *FP(-32*2),a0,1     ; source texture starting linear address
texture_row_loop:
    move    a0,a1   ; a1 is temp linear address of source
    add     b10,b12 ; add step to texel position
    move    b12,a2  ; convert texel X position...
    srl     16,a2   ; ...into an integer...
    sll     3,a2    ; ...and multiply it by 8
    add     a2,a1   ; add pixel X offset to a1 linear address
    move    *a1,a3,0    ; read the pixel byte
    pixt    a3,*a4.xy
    addk    1,a4    ; add 1 to a4, advance 

    cmp     b12,b14 ; compare texel X to size X
    jrhi    texture_row_loop

    ; proceed until out of rows
    .globl texture_advance_row
texture_advance_row:
    move    a11,a4
    andi    0FFFF0000h,a5       ; reset destination X
    addi    000010000h,a5       ; advance one destination Y
    or      a5,a4               ; pack destination coordinate into A4

    ; step along texture Y
    add     b11,b13 ; add Y step to texel Y
    move    b13,a2  ; convert texel Y...
    srl     16,a2   ; ...into an integer...

    ; end condition: end if offset Y eq source Y
    cmp     a2,a10
    jrlo    texture_scaling_done

    ; ok, more to draw.
    sll     9,a2    ; multiply by 64, multiply by 8
    move    a9,a0   ; save the base of the source
    add     a2,a0   ; add that to the starting linear address of the current row

    clr     b12   ; reset texel X to 0
    jruc    texture_row_loop    ; back to the row loop

texture_scaling_done:
    ; dest pitch should be the same, always
        
    setf    32,0,0
    move    *SP(32*13),STK,1     ; restore caller STK
    mmfm	SP,a0,a1,a2,a3,a4,a5,a6,a7,a9,a10,a11,FP   ; restore SP, [any registers that will be modified], FP
	rets    2

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    .globl  _draw_textured_wall_slice
_draw_textured_wall_slice:
    mmtm	SP,a0,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,FP   ; save SP, [any registers that will be modified], FP
	move	STK,FP              ; stack frame
    
    ;move    *FP(-32*1),a0,1     ; param 1   - ptr destination framebuffer (OFFSET)
    ;move    *FP(-32*2),a1,1     ; param 2   - ptr source texture address (SADDR)
    ;move    *FP(-32*3),a2,1     ; param 3   - X coordinate to use from source
    ;move    *FP(-32*4),a3,1     ; param 4   - uint16_t source Y size
    ;move    *FP(-32*5),a4,1     ; param 5   - int16_t destination X
    ;move    *FP(-32*6),a5,1     ; param 6   - int16_t destination Y
    ;move    *FP(-32*7),a6,1     ; param 7   - uint16_t destination Y size

    ; Calculate Y step
    move    *FP(-32*4),a1,1     ; param 4   - uint16_t source Y size
    move    *FP(-32*7),a3,1     ; param 7   - uint16_t destination Y size
    calla   _FX_DIV_reg         ; A8 = texel Y step

    ; Textures are 64x64
    ; Advance source pointer to the correct X slice
    move    *FP(-32*3),a0,1     ; param 3   - X coordinate to use from source
    sll     3,a0                ; multiply by 8 to produce a byte offset
    move    *FP(-32*2),a1,1     ; param 2   - ptr source texture address (SADDR)
    add     a0,a1               ; a1 is the source address
    
    clr     a12                 ; A12 = current texel Y

    move    *FP(-32*5),a4,1     ; param 5   - int16_t destination X
    move    *FP(-32*6),a5,1     ; param 6   - int16_t destination Y
    sll     16,a5               
    or      a4,a5               ; A5 is now a packed XY coordinate

    move    *FP(-32*4),a6,1     ; param 4   - uint16_t source Y size

    clr     a9
    
_draw_slice_loop:
    move    a1,a11              ; A11 = source to read from
    sll     9,a9                ; 64 pixels * 8 bytes
    add     a9,a11              ; add Y offset to source address
    move    *a11,a10            ; read pixel into A10
    pixt    a10,*a5.xy          ; write pixel to destination address
    addi    000010000h,a5       ; advance one destination Y

    ; step along texture Y
    add     a8,a12 ; add Y step to texel Y
    move    a12,a9  ; convert texel Y...
    srl     16,a9   ; ...into an integer...

    ; end condition: end if offset Y eq source Y
    cmp     a9,a6
    jrhi    _draw_slice_loop

    setf    32,0,0
    move    *SP(32*15),STK,1     ; restore caller STK
    mmfm	SP,a0,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,FP   ; restore SP, [any registers that will be modified], FP
	rets    2