#include "fastgraf/prims/drawfont.h"

extern uint16_t *sweet16;

void Put8x16FontChar(uint16_t **source, uint8_t c, int16_t destX, int16_t destY, uint16_t bg, uint16_t fg)
{
	PACKED_XY packed_glyph_size, packed_coordinates;
	struct XY glyph_size, coordinates;

	SetXY(&glyph_size, 8, 16);
	PackXY(&packed_glyph_size, &glyph_size);

	SetXY(&coordinates, destX, destY);
	PackXY(&packed_coordinates, &coordinates);

	pixblt_binary_to_xy(source + (4*c), 8, packed_coordinates, PITCH_512, packed_glyph_size, bg, fg);
}

void PutDebugString(char *string, int16_t x, int16_t y, uint16_t color)
{
	int i=0;

	for(i=0; i<strlen(string); i++)
	{
		Put8x16FontChar(&sweet16, string[i], x, y, 0, color);
		x += 8;
	}	
}
