#include "fastgraf/prims/drawoval.h"

void OvalPlotQuadrant(struct FGState *state, struct XY *center, uint32_t deltaX, uint32_t deltaY, struct CLIP_COORDINATES *clipping, enum FILL_SHAPE fill, enum QUADRANT quadrant)
{
	PACKED_XY packed_p;	
	struct XY from, to;

    if(quadrant & QUADRANT_I)
    {
        if(clipping != 0L)
        {
            if(
                ((center->x + deltaX) < clipping->left_bound)  ||
                ((center->x + deltaX) > clipping->right_bound) ||
                ((center->y - deltaY) < clipping->upper_bound) ||
                ((center->y - deltaY) > clipping->lower_bound))
            {
                /* clipped, don't draw */
            }
            else
            {
                PackXY_Coordinates(&packed_p, center->x + deltaX, center->y - deltaY);
	            plot_pixel(state->pen_state.pattern, packed_p);
            }
        }
        else
        {
            PackXY_Coordinates(&packed_p, center->x + deltaX, center->y - deltaY);
            plot_pixel(state->pen_state.pattern, packed_p);
        }
    }

    if(quadrant & QUADRANT_II)
    {
        if(clipping != 0L)
        {
            if(((center->x - deltaX) < clipping->left_bound) ||
            ((center->x - deltaX) > clipping->right_bound) ||
            ((center->y - deltaY) < clipping->upper_bound) ||
            ((center->y - deltaY) > clipping->lower_bound))
            {
                /* clipped, don't draw */
            }
            else
            {
                PackXY_Coordinates(&packed_p, center->x - deltaX, center->y - deltaY);
                plot_pixel(state->pen_state.pattern, packed_p);
            }
        }
        else
        {
            PackXY_Coordinates(&packed_p, center->x - deltaX, center->y - deltaY);
            plot_pixel(state->pen_state.pattern, packed_p);
        }
    }

    if(quadrant & QUADRANT_III)
    {
        if(clipping != 0L)
        {
            if(((center->x - deltaX) < clipping->left_bound) ||
            ((center->x - deltaX) > clipping->right_bound) ||
            ((center->y + deltaY) < clipping->upper_bound) ||
            ((center->y + deltaY) > clipping->lower_bound))
            {
                /* clipped, don't draw */
            }
            else
            {
                PackXY_Coordinates(&packed_p, center->x - deltaX, center->y + deltaY);
                plot_pixel(state->pen_state.pattern, packed_p);
            }
        }
        else
        {
            PackXY_Coordinates(&packed_p, center->x - deltaX, center->y + deltaY);
            plot_pixel(state->pen_state.pattern, packed_p);
        }
    }

    if(quadrant & QUADRANT_IV)
    {
        if(clipping != 0L)
        {
            if(((center->x + deltaX) < clipping->left_bound) ||
            ((center->x + deltaX) > clipping->right_bound) ||
            ((center->y + deltaY) < clipping->upper_bound) ||
            ((center->y + deltaY) > clipping->lower_bound))
            {
                /* clipped, don't draw */
            }
            else
            {
                PackXY_Coordinates(&packed_p, center->x + deltaX, center->y + deltaY);
                plot_pixel(state->pen_state.pattern, packed_p);
            }
        }
        else
        {
            PackXY_Coordinates(&packed_p, center->x + deltaX, center->y + deltaY);
            plot_pixel(state->pen_state.pattern, packed_p);
        }
    }

	if(fill == FILL_YES)
	{
		SetXY(&from, center->x - deltaX, center->y + deltaY);
		SetXY(&to, center->x + deltaX, center->y + deltaY);
		LineFromTo(&from, &to, state->pen_state.pattern);

		SetXY(&from, center->x - deltaX, center->y - deltaY);
		SetXY(&to, center->x + deltaX, center->y - deltaY);
		LineFromTo(&from, &to, state->pen_state.pattern);
	}
}

void OvalPointCalculations(struct FGState *state, struct Rect *r, struct XY *center, struct CLIP_COORDINATES *clipping, enum FILL_SHAPE fill, enum QUADRANT quadrant)
{
	/* https://www.ece.k-state.edu/people/faculty/carpenter/documents/midellipr.pdf */
	int32_t a = r->w / 2;
	int32_t b = r->h / 2;

	int32_t x = 0,
				y = b;

	int32_t a2 = r->w * r->w, /* squares of axes */
				b2 = r->h * r->h; 
	int32_t h, d1, d2, sn, sd; /* decision variables */
	int32_t a2t8 = a2 << 3, b2t8 = b2 << 3;

	/* initialize for first midpoint */
	h = (b2t8 >> 1) + a2*(1 - (b << 2));
	d1 = 12*b2;
	d2 = -a2t8 * (b-1);

	sn = b2;
	sd = (a2*b) - (a2 >> 1);

	/* begin loop for incrementing X */
	while(sn < sd) 	/* magnitude of slope less than 1 */
	{
		if(h > 0)	/* midpoint is outside ellipse */
		{
			y--;
			h += d2;
			sd -= a2;
			d2 += a2t8;
		}
		x++;		/* always increment these four values */
		h += d1;
		sn += b2;
		d1 += b2t8;

		OvalPlotQuadrant(state, center, x, y, clipping, fill, quadrant);
	}

	/* 
		Here, before continuing with decrementing Y,
		must initialize the decision variable for
		the new midpoints by changing the values
		of h, d1, and d2 for X greater than 0.5 
		and Y less than 0.5
	*/

	h -= a2*((y << 2) - 3) + b2*((x << 2) + 3);
	d1 -= (b2 << 1);
	b2 -= (a2 << 1);

	while(y > 1) 	/* continue with decrementing Y */
	{
		if(h < 0)
		{
			x++;
			h += d1;
			d1 += b2t8;
		}
		y--;		/* always decrement y, h, and d2 */
		h += d2;
		d2 += a2t8;

		OvalPlotQuadrant(state, center, x, y, clipping, fill, quadrant);
	}
}

void FrameOval(struct FGState *state, struct Rect *r)
{
	/* Outline an oval described by the enclosing rectangle R. */

	struct XY midpoint_horiz_l, midpoint_horiz_r;	/* left and right */
	struct XY midpoint_vert_u, midpoint_vert_d;		/* up and down */
	struct XY center;

	PACKED_XY packed_p;

	SetXY(&center, r->x + (r->w >> 1), r->y + (r->h >> 1));
	SetXY(&midpoint_horiz_l, r->x, r->y + (r->h / 2));
	SetXY(&midpoint_horiz_r, r->x + r->w, r->y + (r->h / 2));
	SetXY(&midpoint_vert_u, r->x + (r->w / 2), r->y);
	SetXY(&midpoint_vert_d, r->x + (r->w / 2), r->y + r->h);

	PackXY(&packed_p, &midpoint_horiz_l);
	plot_pixel(state->pen_state.pattern, packed_p);
	PackXY(&packed_p, &midpoint_horiz_r);
	plot_pixel(state->pen_state.pattern, packed_p);
	PackXY(&packed_p, &midpoint_vert_u);
	plot_pixel(state->pen_state.pattern, packed_p);
	PackXY(&packed_p, &midpoint_vert_d);
	plot_pixel(state->pen_state.pattern, packed_p);

	OvalPointCalculations(state, r, &center, NULL, FILL_NO, QUADRANT_ALL);
}

void PaintOval(struct FGState *state, struct Rect *r)
{
	/* Outline an oval described by the enclosing rectangle R. */

	struct XY midpoint_horiz_l, midpoint_horiz_r;	/* left and right */
	struct XY midpoint_vert_u, midpoint_vert_d;		/* up and down */
	struct XY center;

	PACKED_XY packed_p;

	SetXY(&center, r->x + (r->w >> 1), r->y + (r->h >> 1));
	SetXY(&midpoint_horiz_l, r->x, r->y + (r->h / 2));
	SetXY(&midpoint_horiz_r, r->x + r->w, r->y + (r->h / 2));
	SetXY(&midpoint_vert_u, r->x + (r->w / 2), r->y);
	SetXY(&midpoint_vert_d, r->x + (r->w / 2), r->y + r->h);

	PackXY(&packed_p, &midpoint_horiz_l);
	plot_pixel(state->pen_state.pattern, packed_p);
	PackXY(&packed_p, &midpoint_horiz_r);
	plot_pixel(state->pen_state.pattern, packed_p);
	PackXY(&packed_p, &midpoint_vert_u);
	plot_pixel(state->pen_state.pattern, packed_p);
	PackXY(&packed_p, &midpoint_vert_d);
	plot_pixel(state->pen_state.pattern, packed_p);

	OvalPointCalculations(state, r, &center, NULL, FILL_YES, QUADRANT_ALL);
	LineFromTo(&midpoint_horiz_l, &midpoint_horiz_r, state->pen_state.pattern);
}