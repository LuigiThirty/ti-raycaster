    .global _prim_fill_rect

    .text

    .include "include\tms34010.equ"

_prim_fill_rect:
    mmtm	SP,a0,a1,a2,a3,FP     ; save SP, [any registers that will be modified], FP
	move	STK,FP          ; stack frame

    move    *FP(-32),a0,1   ; param 1   - void* linear buffer
    move	*FP(-64),a1,1   ; param 2   - PACKED_XY origin
	move	*FP(-96),a2,1   ; param 3   - PACKED_XY size
    move    *FP(-128),a3,1   ; param 4   - uint16_t pattern

    move    a0,OFFSET
    move    a1,DADDR
    move    a2,DYDX
    move    a3,COLOR1
    nop
    fill    xy
    nop

    move    *SP(32*6),STK,1         ; restore caller STK
    mmfm	SP,a0,a1,a2,a3,FP  ; restore SP, [any registers that were modified], FP
    rets    2