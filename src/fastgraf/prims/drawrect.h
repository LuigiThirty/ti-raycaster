#ifndef DRAWRECT_H
#define DRAWRECT_H

#include <stdint.h>

#include "v_video.h"
#include "fastgraf/types/rect.h"
#include "fastgraf/state.h"

void FrameRect(struct FGState *state, struct Rect *r);
void FillRect(struct FGState *state, struct Rect *r);

/* prim */
extern void prim_fill_rect(void *buffer, PACKED_XY origin, PACKED_XY size, PEN_PATTERN pattern);

#endif