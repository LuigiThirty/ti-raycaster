#include "fastgraf/prims/drawtri.h"

void FrameTriangle(struct FGState *state, struct Triangle *t)
{
    /* Draw a line from p1 to p2, p2 to p3, p3 to p1 */
    LineFromTo(state, &t->p1, &t->p2);
    LineFromTo(state, &t->p2, &t->p3);
    LineFromTo(state, &t->p1, &t->p3);
}