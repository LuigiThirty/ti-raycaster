#ifndef FASTGRAF_PRIMS_DRAWOVAL_H
#define FASTGRAF_PRIMS_DRAWOVAL_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "fastgraf/state.h"
#include "fastgraf/types/rect.h"
#include "fastgraf/types/xy.h"

enum QUADRANT
{
    QUADRANT_I      = 1,
    QUADRANT_II     = 2,
    QUADRANT_III    = 4,
    QUADRANT_IV     = 8
};

#define QUADRANT_ALL (QUADRANT_I|QUADRANT_II|QUADRANT_III|QUADRANT_IV)

struct CLIP_COORDINATES
{
    int16_t left_bound, right_bound, upper_bound, lower_bound;
};

extern void plot_pixel(uint8_t color, PACKED_XY coordinate);

void OvalPointCalculations(struct FGState *state, struct Rect *r, struct XY *center, struct CLIP_COORDINATES *clipping, enum FILL_SHAPE fill, enum QUADRANT quadrant);
void OvalPlotQuadrant(struct FGState *state, struct XY *center, uint32_t deltaX, uint32_t deltaY, struct CLIP_COORDINATES *clipping, enum FILL_SHAPE fill, enum QUADRANT quadrant);

void FrameOval(struct FGState *state, struct Rect *r);
void PaintOval(struct FGState *state, struct Rect *r);

#endif