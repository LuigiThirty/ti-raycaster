#ifndef FASTGRAF_PEN_H
#define FASTGRAF_PEN_H

#include <string.h>
#include "fastgraf/state.h"

/* FastGraf graphics pen routines. */

void PenNormal(struct FGState *state);

void Move(struct FGState *state, struct XY *delta);
void MoveTo(struct FGState *state, struct XY *to);

void HidePen(struct FGState *state);
void ShowPen(struct FGState *state);

void GetPenState(struct FGState *state, struct FGPenState *outState);
void SetPenState(struct FGState *state, struct FGPenState *inState);

void PenSize(struct FGState *state, struct XY *size);
void PenMode(struct FGState *state, enum PATTERN_MODE mode);
void PenPat(struct FGState *state, PEN_PATTERN pattern);

void GetPen(struct FGState *state, struct XY *p);

#endif