#include "fastgraf/pen.h"

void GetPen(struct FGState *state, struct XY *p)
{
    /* Returns the current pen location. */
    p->x = state->pen_state.location.x;
    p->y = state->pen_state.location.x;
}

void PenNormal(struct FGState *state)
{
    /* Reset all pen parameters except location to defaults. */
    state->pen_state.pattern = 0;
    state->pen_state.pattern_mode = PAT_COPY;
    state->pen_state.size.x = 1;
    state->pen_state.size.y = 1;
}

void Move(struct FGState *state, struct XY *delta)
{
    /* Move the pen to a relative location. */
    state->pen_state.location.x += delta->x;
    state->pen_state.location.y += delta->y;
}

void MoveTo(struct FGState *state, struct XY *to)
{
    /* Move the pen to an absolute location. */
    state->pen_state.location.x = to->x;
    state->pen_state.location.y = to->y;
}

void HidePen(struct FGState *state)
{
    /* Decrement the pen visibility count. If negative, draws are not performed. */
    state->pen_visible--;
}

void ShowPen(struct FGState *state)
{
    /* Increment the pen visibility count. If negative, draws are not performed. */
    state->pen_visible++;
}

void GetPenState(struct FGState *state, struct FGPenState *outState)
{
    /* Fetch the pen_state. */
    memcpy(outState, &state->pen_state, sizeof(struct FGPenState));
}

void SetPenState(struct FGState *state, struct FGPenState *inState)
{
    /* Copy a new pen_state into the FGState. */
    memcpy(&state->pen_state, inState, sizeof(struct FGPenState));
}

void PenSize(struct FGState *state, struct XY *size)
{
    /* Sets the pen size. */
    memcpy(&state->pen_state.size, size, sizeof(struct XY));
}

void PenMode(struct FGState *state, enum PATTERN_MODE mode)
{
    /* Update the pen's pattern bitmap mode. */
    state->pen_state.pattern_mode = mode;
}

void PenPat(struct FGState *state, PEN_PATTERN pattern)
{
    /* Updates the pen's pattern word. */
    state->pen_state.pattern = pattern;
}