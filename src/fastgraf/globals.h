#ifndef FASTGRAF_GLOBALS_H
#define FASTGRAF_GLOBALS_H

#define M_PI   3.14159265358979323846264338327950288

#define PITCH_512 512*8

#define degToRad(angleInDegrees) ((angleInDegrees) * M_PI / 180.0)
#define radToDeg(angleInRadians) ((angleInRadians) * 180.0 / M_PI)

#define SCREEN_HEIGHT 240
#define SCREEN_WIDTH 320

#endif