#include "fastgraf/types/rect.h"

void SetRect(struct Rect *r, int16_t x, int16_t y, int16_t w, int16_t h)
{
    r->x = x;
    r->y = y;
    r->w = w;
    r->h = h;
}