#ifndef FIXEDPT_H
#define FIXEDPT_H

#include <math.h>
#include <stdint.h>

/* S15.16 fixed point math library. */
/* All numbers are signed 15-bit magnitude and 16-bit fraction. */

typedef int32_t FIXED;

struct FX_VECTOR2 {
    FIXED x;
    FIXED y;
};

#define Q 15

/* 
    Fixed-point addition and subtraction with numbers of the same precision
    is the same as conventional addition and subtraction.

    This does not take overflows into account.
*/
#define FX_ADD(A,B) ( A + B )
#define FX_SUB(A,B) ( A - B )

#define FX_FROMINT(A) ( A << 16 )
#define FX_TOINT(A) ( A >> 16 )

#define FX_FROMFLOAT(A) ((int)(floor(( A * 65536.0 )+0.5)))
/* #define FX_TOFLOAT(A) (((float)A) / 65536.0) */

extern FIXED FX_MUL(FIXED a, FIXED b);
extern FIXED FX_DIV(FIXED a, FIXED b);

static FIXED FX_FastMUL(uint32_t a, uint32_t b)
{
	MMIO32(0x20000000) = a;
	MMIO32(0x20000020) = b;
	return MMIO32(0x20000040);
}

float FX_TOFLOAT(FIXED a);

#endif