    .include    "include/tms34010.equ"
    .text

    .global _FX_MUL
    .global _FX_DIV

    ; calling convention: return the value in A8
    ; calling convention: field size 1 must be 32 when returning to C
    ; calling convention: restore caller stack before returning. *SP(32*N),STK,1 where N is the number of saved registers

_FX_MUL:
	mmtm	SP,a0,a1,FP             ; mmtm SP,<used registers>,FP
	move	STK,FP                  ; grab the FP

    move	*FP(-32*1),a0,1         ; A
	move	*FP(-32*2),a1,1         ; B

    mpys    a1,a0                   ; A1 * A0, result C is A0:A1, 32-bit FS1 is implied
                                    ; the result is now a S31.32 number, so convert it to S15.16.
                                    ; we need the middle 32 bits from the result.
    srl     16,a1                   ; AAAABBBB:0000CCCC
    sll     16,a0                   ; BBBB0000:0000CCCC
    or      a0,a1                   ; BBBB0000:BBBBCCCC
    move    a1,a8                   ; now in the result register
    
    move    *SP(32*4),STK,1         ; restore caller STK
    mmfm	SP,a0,a1,FP    ; mmfm SP,<used registers>,FP
    rets 2

_FX_DIV:
	mmtm	SP,a0,a1,a2,a3,FP    ; mmtm SP,<used registers>,FP
	move	STK,FP               ; grab the STK

    move	*FP(-32*1),a1,1         ; A
	move	*FP(-32*2),a3,1         ; B

    ; Need to shift the dividend left by 16 bits into a0.
    ; It then becomes a0:a1 / a3.
    move    a1,a0
    srl     16,a0                   ; 0000AAAA:AAAABBBB
    sll     16,a1                   ; 0000AAAA:BBBB0000

    ; Sign-extend A0 if required.
    btst    15,a0
    jreq    _fx_div_calc ; Z=0 if bit is 1
_fx_div_sext:
    ori     0FFFF0000h,a0

_fx_div_calc:
    divs    a3,a0                   ; Rd / Rs, so 0000AAAA:BBBB0000 / CCCCDDDD
    move    a0,a8

    move    *SP(32*6),STK,1         ; restore caller STK
    mmfm	SP,a0,a1,a2,a3,FP       ; mmfm SP,<used registers>,FP
    rets 2
