#ifndef TRI_H
#define TRI_H

#include <stdint.h>

#include "fastgraf/types/xy.h"

struct Triangle {
    struct XY p1, p2, p3;
};

void SetTriangle(struct Triangle *t, int16_t p1x, int16_t p1y,
                                     int16_t p2x, int16_t p2y, 
                                     int16_t p3x, int16_t p3y);

#endif
