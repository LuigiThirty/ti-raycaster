#ifndef XY_H
#define XY_H

#include <stdint.h>

typedef uint32_t PACKED_XY;	/* High 16 bits are Y, low 16 bits are X */

enum FILL_SHAPE {
	FILL_YES,
	FILL_NO
};

struct XY
{
	int16_t x, y;
};

void SetXY(struct XY *xy, int16_t x, int16_t y);
void PackXY(PACKED_XY *packed, struct XY *xy);
void PackXY_Coordinates(PACKED_XY *packed, int16_t x, int16_t y);

#endif