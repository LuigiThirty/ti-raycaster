#include "fastgraf/types/xy.h"

void SetXY(struct XY *xy, int16_t x, int16_t y)
{
		xy->x = x;
		xy->y = y;
}

void PackXY(PACKED_XY *packed, struct XY *xy)
{
	*packed = xy->y;
	*packed = *packed << 16;
	*packed |= xy->x;	
}

void PackXY_Coordinates(PACKED_XY *packed, int16_t x, int16_t y)
{
	*packed = y;
	*packed = *packed << 16;
	*packed |= x;	
}