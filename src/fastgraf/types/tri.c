#include "fastgraf/types/tri.h"

void SetTriangle(struct Triangle *t, int16_t p1x, int16_t p1y,
                                     int16_t p2x, int16_t p2y, 
                                     int16_t p3x, int16_t p3y)
{
    t->p1.x = p1x; t->p1.y = p1y;
    t->p2.x = p2x; t->p2.y = p2y;
    t->p3.x = p3x; t->p3.y = p3y;
}