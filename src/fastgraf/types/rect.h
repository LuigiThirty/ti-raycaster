#ifndef RECT_H
#define RECT_H

#include <stdint.h>

struct Rect {
    int16_t x, y, w, h;
};

void SetRect(struct Rect *r, int16_t x, int16_t y, int16_t w, int16_t h);

#endif
