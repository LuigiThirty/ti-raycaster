#ifndef FASTGRAF_STATE_H
#define FASTGRAF_STATE_H

#include "fastgraf/types/xy.h"

typedef uint32_t PEN_PATTERN;

enum PATTERN_MODE
{
    PAT_COPY,
    PAT_OR,
    PAT_XOR,
    PAT_BIC,
    PAT_NOTPATCOPY,
    PAT_NOTPATOR,
    PAT_NOTPATXOR,
    PAT_NOTPATBIC
};

struct FGPenState
{
    struct XY location;     /* Draws are UP AND LEFT of the invisible pen point. */
    struct XY size;         /* In pixels. */
    enum PATTERN_MODE pattern_mode;   /* Pen pattern mode */
    PEN_PATTERN pattern;    /* Pen pattern */
};

struct FGState
{
    struct FGPenState pen_state;
    struct XY window_size;

    int8_t pen_visible;                 /* If negative, drawing is not visible. */

    /* Double buffering. */
    void *front_buffer;
    void *back_buffer;

    uint16_t front_DPYSTRT;
    uint16_t back_DPYSTRT;
};

void InitFGState(struct FGState *state);
void SwapBuffers(struct FGState *state);

extern void ResetPitchAndWindow();
extern void Update_DPYSTRT(uint16_t val);

#endif