    .text

    .include "include\tms34010.equ"
    
    .global _Update_DPYSTRT
_Update_DPYSTRT:
    mmtm	SP,a0,FP        ; save SP, [any registers that will be modified], FP
	move	STK,FP          ; stack frame
    move    *FP(-32),a0,1   ; param 1   - uint16_t DPYSTRT
    setf    16,0,0
    move    a0,@DPYSTRT,0
    
    move    *SP(32*3),STK,1         ; restore caller STK
    mmfm	SP,a0,FP  ; restore SP, [any registers that were modified], FP
    rets    2
