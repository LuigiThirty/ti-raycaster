#ifndef CTEST_H
#define CTEST_H

#include <gspreg.h>
#include <gsptypes.h>

#include <stdlib.h>
#include <stdint.h>

#include "loadpal.h"
#include "framelp.h"
#include "v_video.h"

void InitVideoRegisters();
void ClearVRAM();

#endif