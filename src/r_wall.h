#ifndef R_WALL_H
#define R_WALL_H

#include <stdint.h>

extern void draw_textured_wall_slice(void *buffer, uint8_t *source, uint16_t slice_x, uint16_t size_y, int16_t dest_x, int16_t dest_y, uint16_t dest_size_y);

#endif