#ifndef VIDEO_H
#define VIDEO_H

#include <gspreg.h>
#include <gsptypes.h>

#include <stdlib.h>
#include <stdint.h>

#include "fastgraf/state.h"

void WaitForDIFlag();
void InitVideoRegisters();
extern void Set_COLOR1(PEN_PATTERN pattern);

#endif