#ifndef SERIAL_H
#define SERIAL_H

#include <stdint.h>

#define SIOA_C 0x02000000
#define SIOA_D 0x02000010
#define SIOB_C 0x02000020
#define SIOB_D 0x02000030

void SER_Init();
void SER_PutChar(uint8_t c);

void _putchar(char character);

#endif