#include "g_entry.h"
#include "sys_uart.h"
#include "util/printf.h"

#include "w_wadmgr.h"

int main()
{
	struct WAD_Directory *doompal;

	SER_Init();
	printf("34010 Monitor - Dr. Pixel is on duty\r\n");
	printf("Init video registers\r\n");
	InitVideoRegisters();

	W_LoadBundle();
	doompal = W_GetLump("DOOMPAL");
	LoadPaletteC((uint8_t *)W_GetLumpDataPtr(doompal), 255, 0);
	printf("Clearing VRAM\r\n");
	ClearVRAM();

	FrameStart();
	return 0;
}

void ClearVRAM()
{
	uint32_t i;
	uint8_t *current;
	
	i = 0;
	current = 0;
	
	for(i=0; i<512*240; i++)
	{		
		*current++ = 0;
	}
}

