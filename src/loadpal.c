#include <stdint.h>
#include "loadpal.h"

#include "util/printf.h"

/* TODO: Static variables aren't getting copied into BSS. Why? */
void LoadPaletteC(uint8_t *paldata, uint8_t palsize, uint8_t palindex)
{
	/* 
	 * paldata:  ptr to RGB888 palette
	 * palsize:  number of entries to load
	 * palindex: '76 index to start loading at
	 */
	int i;

	printf("Loading palette from %08X\n", paldata);
	
	MMIO8(PALINDEX) = palindex;	/* set the '76 palette index */

	for(i=0; i<palsize; i++)
	{
		MMIO8(PALWRITE) = paldata[(i*3)+0] >> 2;
		MMIO8(PALWRITE) = paldata[(i*3)+1] >> 2;
		MMIO8(PALWRITE) = paldata[(i*3)+2] >> 2;
	}
	
	/* Make sure color 255 is white. */
	MMIO8(PALINDEX) = 255;
	MMIO8(PALWRITE) = 255;
	MMIO8(PALWRITE) = 255;
	MMIO8(PALWRITE) = 255;
}