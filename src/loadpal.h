#ifndef LOADPAL
#define LOADPAL

#define PALINDEX 0x1000000
#define PALWRITE 0x1000010

extern uint8_t EGAPalette[256*3];
extern uint8_t DOOMPal[256*3];
void LoadPaletteC(uint8_t *paldata, uint8_t palsize, uint8_t palindex);

#endif