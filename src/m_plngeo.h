#ifndef M_PLNGEO_H
#define M_PLNGEO_H

/* Plane geometry functions. */
#include "fixedpt.h"

FIXED DistanceBetweenPoints(struct FX_VECTOR2 a, struct FX_VECTOR2 b);

#endif