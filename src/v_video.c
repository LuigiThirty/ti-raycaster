#include "v_video.h"

extern uint16_t DIFlag;

void WaitForDIFlag()
{
	/* Loop until DIFlag is set, meaning we're in VBLANK. */
	/* If in VBLANK already, wait for the next full VBLANK. */
	if(DIFlag == 1) DIFlag = 0;
	while(DIFlag == 0) {}
}

void InitVideoRegisters()
{
	/* Set up all the video registers for one 320x240x8 screen at $0 */
	register convdp_result;	/* A9 */
	
	/* CONVDP is a calculated value using the LMO instruction. */
	convdp_result 		= 0x1000;
	asm("	lmo a9,a9");
	MMIO16(CONVDP)		= convdp_result;
	
	MMIO16(HESYNC10) 	= 0x26;		/* H. End Sync */
	MMIO16(HEBLNK10) 	= 0x5C;		/* H. End Blank */
	MMIO16(HSBLNK10) 	= 0x19C;	/* H. Start Blank */
	MMIO16(HTOTAL10)	= 0x1BB;	/* H. Pixel Clocks */
	MMIO16(HCOUNT10)	= 0x5C;		/* H. Counter */
	
	MMIO16(VESYNC10) 	= 0x4;		/* V. End Sync */
	MMIO16(VEBLNK10) 	= 0x18;		/* V. End Blank */
	MMIO16(VSBLNK10) 	= 0x108;	/* V. Start Blank */
	MMIO16(VTOTAL10) 	= 0x110;	/* V. Total Scanlines */
	MMIO16(VCOUNT10)	= 0x108;	/* V. Scanline Counter */
	
	MMIO16(DPYCTL) 		= 0xF010;	/* Display Control */
	MMIO16(DPYSTRT) 	= 0xFFFC;	/* Display Start Address */
	MMIO16(CONTROL)		= 0x0104;	/* Control Register */
	
	MMIO16(PSIZE) 		= 0x8;		/* Pixel Size */
	MMIO16(PMASK)		= 0x0;		/* Plane Mask */

	MMIO16(DPYINT)		= 264;		/* Scanline Interrupt */
	MMIO16(INTENB)		= 0x0400;	/* Interrupt Enable Bits */
}

