#include "r_raycst.h"

#define MAP_WIDTH 24
#define MAP_HEIGHT 24

struct RotationSinCos rotationTable;

extern uint8_t worldmap[24][24];

void R_CalculateRotationSpeeds(struct RotationSinCos *table, float rotSpeed)
{
	printf("Populating rotation table: rotSpeed %f\n", rotSpeed);

	table->right_cos_rotSpeed = FX_FROMFLOAT((float)cos(rotSpeed));
	table->right_sin_rotSpeed = FX_FROMFLOAT((float)sin(rotSpeed));
	table->right_negative_cos_rotSpeed = FX_FROMFLOAT((float)cos(-rotSpeed));
	table->right_negative_sin_rotSpeed = FX_FROMFLOAT((float)sin(-rotSpeed));

	table->left_cos_rotSpeed = FX_FROMFLOAT((float)cos(-rotSpeed));
	table->left_sin_rotSpeed = FX_FROMFLOAT((float)sin(-rotSpeed));
	table->left_negative_cos_rotSpeed = FX_FROMFLOAT((float)cos(rotSpeed));
	table->left_negative_sin_rotSpeed = FX_FROMFLOAT((float)sin(rotSpeed));
}


void R_DDA(struct R_RaycasterState *r_state)
{
	bool hit = 0;

	register FIXED r_fxSideDistX 	= r_state->side_distance.x;
	register FIXED r_fxSideDistY 	= r_state->side_distance.y;
	register FIXED r_fxDeltaDistX	= r_state->delta_distance.x;
	register FIXED r_fxDeltaDistY 	= r_state->delta_distance.y;
	register FIXED r_fx_mapX 		= r_state->map_position.x;
	register FIXED r_fx_mapY 		= r_state->map_position.y;
	register FIXED r_fx_stepX 		= r_state->step.x;
	register FIXED r_fx_stepY 		= r_state->step.y;

	while (hit == 0)
	{
		/* jump to next map square, OR in x-direction, OR in y-direction */
		if (r_fxSideDistX < r_fxSideDistY)
		{
			r_fxSideDistX += r_fxDeltaDistX;
			r_fx_mapX += r_fx_stepX;
			r_state->side = 0;
		}
		else
		{
			r_fxSideDistY += r_fxDeltaDistY;
			r_fx_mapY += r_fx_stepY;
			r_state->side = 1;
		}
		/* Check if ray has hit a wall. */
		if (worldmap[FX_TOINT(r_fx_mapX)][FX_TOINT(r_fx_mapY)] > 0) hit = 1;
	}

	r_state->side_distance.x 	= r_fxSideDistX;
	r_state->side_distance.y 	= r_fxSideDistY;
	r_state->delta_distance.x 	= r_fxDeltaDistX;
	r_state->delta_distance.y 	= r_fxDeltaDistY;
	r_state->map_position.x		= r_fx_mapX;
	r_state->map_position.y		= r_fx_mapY;
	r_state->step.x 			= r_fx_stepX;
	r_state->step.y 			= r_fx_stepY;
}