	.include "prims.inc"

	.text

FP	.set	A13
STK .set	A14

	.global	_Set_COLOR1
	.global _ResetPitchAndWindow
_Set_COLOR1:
	mmtm	SP,a7,FP
	move	STK,FP
	move	*FP(-32),a7,1
	move	a7,b9
	move    *SP(32*3),STK,1         ; restore caller STK
	mmfm	SP,a7,FP
	rets	2

_ResetPitchAndWindow:
	movi	PITCH_512,DPTCH			; Pixel operations use several predefined registers in file B.
	movi	0,OFFSET				; Framebuffer starts at $0 in VRAM.
	movi	0,WSTART				; Window starts at [0,0].
	movi	WINDOW_320x240,WEND		; Define a 320x200 window to match the screen's visible area.
	rets

	.global	_pixblt_binary_to_linear
_pixblt_binary_to_linear:
	; Binary pixel array -> linear pixel array (with processing)
	; pg. 12-174 of manual

	mmtm	SP,a0,a1,a2,a3,a4,a5,a6,FP
	move	STK,FP
	move	*FP(-32*1),a0,1
	move	*FP(-32*2),a1,1
	move	*FP(-32*3),a2,1
	move	*FP(-32*4),a3,1
	move	*FP(-32*5),a4,1
	move	*FP(-32*6),a5,1
	move	*FP(-32*7),a6,1

	move	a0,b0
	move	a1,b1
	move	a2,b2
	move	a3,b3
	move	a4,b7
	move	a5,b8
	move	a6,b9

	; TODO: set CONTROL
	; destination pixel size should always be 8
	pixblt	b,l

    move    *SP(32*9),STK,1         ; restore caller STK
	mmfm	SP,a0,a1,a2,a3,a4,a5,a6,FP  ; restore SP, [any registers that were modified], FP
    rets    2

	.global	_pixblt_binary_to_xy
_pixblt_binary_to_xy:
	; Binary pixel array -> XY pixel array (with processing)
	; pg. 12-174 of manual

	mmtm	SP,a0,a1,a2,a3,a4,a5,a6,FP
	move	STK,FP
	move	*FP(-32*1),a0,1
	move	*FP(-32*2),a1,1
	move	*FP(-32*3),a2,1
	move	*FP(-32*4),a3,1
	move	*FP(-32*5),a4,1
	move	*FP(-32*6),a5,1
	move	*FP(-32*7),a6,1

	move	a0,b0
	move	a1,b1
	move	a2,b2
	move	a3,b3
	move	a4,b7
	move	a5,b8
	move	a6,b9

	lmo		a1,a1	; CONVSP
	setf	16,0,0
	move	a1,@CONVSP

	lmo		a3,a3	; CONVDP
	setf	16,0,0
	move	a3,@CONVDP

	; TODO: set CONTROL
	; destination pixel size should always be 8
	pixblt	b,xy

    move    *SP(32*9),STK,1         ; restore caller STK
	mmfm	SP,a0,a1,a2,a3,a4,a5,a6,FP  ; restore SP, [any registers that were modified], FP
    rets    2