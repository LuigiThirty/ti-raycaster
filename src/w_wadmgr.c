#include "w_wadmgr.h"

struct WAD_Header header;
struct WAD_Directory *directory;

void W_LoadBundle()
{
    int i=0;
    printf("W_LoadBundle: BUNDLE.WAD is at %08X\n", &bundle);
    memcpy(&header, &bundle, 12);

    printf("W_LoadBundle: BUNDLE contains %d entries at offset %08X.\n", header.directory_entries, (uint32_t)&bundle + ((uint32_t)header.directory_start * 8));

    directory = (struct WAD_Directory *)((uint32_t)&bundle + ((uint32_t)header.directory_start * 8));

    for(i=0; i<header.directory_entries; i++)
    {
        char lump[10];
        memcpy(lump, directory[i].lump_name, 8);
        lump[8] = 0;
        printf("%03d - %s\n", i, lump);        
    }
}

void *W_GetWadBase()
{
    return &bundle;
}

uint16_t W_GetLumpCount()
{
    return header.directory_entries;
}

void *W_GetLumpDataPtr(struct WAD_Directory *lump)
{
    /* Returns an absolute pointer to the lump data in ROM. */
    return (void *)((uint32_t)W_GetWadBase() + ((uint32_t)lump->data * 8));
}

void *W_ConvertWadPtrToGlobalPtr(void *data)
{
    return (void *)((uint32_t)W_GetWadBase() + ((uint32_t)data * 8)); 
}

struct WAD_Directory *W_GetLump(char *lumpname)
{
    int i;
    char temp[9];

    int result;

    memset(temp, 0, 9);

    for(i=0; i<header.directory_entries; i++)
    {
        memcpy(temp, directory[i].lump_name, 8);
        if(strcmp(lumpname, temp) == 0)
        {
            return &directory[i];
        }
    }

    return NULL;
}