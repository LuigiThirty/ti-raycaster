#ifndef FRAMELP_C
#define FRAMELP_C

#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "util/printf.h"
#include "r_raycst.h"

#include "v_video.h"
#include "g_world.h"
#include "i_inputs.h"
#include "r_txdata.h"

#include "fastgraf/pen.h"
#include "fastgraf/state.h"
#include "fastgraf/types/fixedpt.h"
#include "fastgraf/prims/drawfont.h"
#include "fastgraf/prims/drawrect.h"
#include "fastgraf/prims/drawtri.h"
#include "fastgraf/prims/drawline.h"
#include "fastgraf/prims/drawoval.h"
#include "fastgraf/prims/drawfont.h"

void FrameStart();
void FrameLoop();

#endif