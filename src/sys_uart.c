#include "sys_uart.h"

void SER_Init()
{
	/* init the SIO */
	/* 115200bps, 8N1, with a 7.37MHz crystal */
	MMIO8(SIOA_C) = 48;
	MMIO8(SIOA_C) = 0x18;
	MMIO8(SIOA_C) = 0x04;
	MMIO8(SIOA_C) = 196;
	MMIO8(SIOA_C) = 0x05;
	MMIO8(SIOA_C) = 232;

	MMIO8(SIOB_C) = 1;

	MMIO8(SIOA_C) = 1;
	MMIO8(SIOA_C) = 0;

	MMIO8(SIOA_C) = 0x3;
	MMIO8(SIOA_C) = 0xC1;
}

void SER_PutChar(uint8_t c)
{
	/*
	rc2014_sio_TX:
		push af
.txb: 	in a,($80)          ; read serial status
        bit 2,a             ; check status bit 2
        jr z, .txb        	; loop if zero (serial is busy)
		*/

	uint8_t serial_status;

	serial_status = MMIO8(SIOA_C);
	while((serial_status & 0x04) == 0) { serial_status = MMIO8(SIOA_C); }
    MMIO8(SIOA_D) = c;
}

void _putchar(char character)
{
	SER_PutChar(character);
}