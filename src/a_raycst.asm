    .global "tms34010.asm"

_R_A_DDA:
    mmtm	SP,a0,FP
    move	*FP(-32),a0,1           ; a0 <- *r_state

    move    *SP(32*3),STK,1         ; restore caller STK
	mmfm	SP,a0,FP
    rets    2