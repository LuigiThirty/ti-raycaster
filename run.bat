C:\msys64\mingw64\bin\objcopy -I ihex -O binary .\out\prog.lo c:\mamesrc/mame/roms/rc2014/prog.lo.bin
C:\msys64\mingw64\bin\objcopy -I ihex -O binary .\out\prog.hi c:\mamesrc/mame/roms/rc2014/prog.hi.bin

@echo Connect terminal program to COM4, make sure MAME's serial port is set to 115200 baud
@pushd c:\mamesrc\mame
mame64 rc2014 -rc2014:1 cf -rc2014:1:cf:ata:0 hdd -hard rc2014.chd -rc2014:3 tms_video -debug -nofilter
@popd