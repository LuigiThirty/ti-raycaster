#!/bin/sh

objcopy -I ihex -O binary prog.lo prog.lo.bin
objcopy -I ihex -O binary prog.hi prog.hi.bin
cp prog.lo.bin /mnt/c/mamesrc/mame/roms/rc2014
cp prog.hi.bin /mnt/c/mamesrc/mame/roms/rc2014
