AS = gspa
CC = gspcl

CFLAGS = -c -ax -v10 -k -i.\include -al -ax -isrc -fr $(dir $@)
ASFLAGS= -i.\include -isrc

AS_CMD = $(AS) $(ASFLAGS) -l $< -o $@
CC_CMD = $(CC) $(CFLAGS) $<

out/prog.out: 	out/hello.obj out/isr.obj out/prims.obj out/loadpal.obj out/framelp.obj out/r_wall.obj out/r_txdata.obj out/w_wadmgr.obj out/v_video.obj out/g_entry.obj out/g_world.obj out/r_raycst.obj out/i_inputs.obj out/sys_uart.obj \
				out/util/printf.obj out/util/json.obj \
				out/fastgraf/prims/drawline.obj out/fastgraf/prims/drawrect.obj out/fastgraf/prims/drawfont.obj out/fastgraf/prims/drawoval.obj out/fastgraf/prims/drawtri.obj \
				out/fastgraf/prims/primline.obj out/fastgraf/prims/primrect.obj out/fastgraf/prims/primpixl.obj \
				out/fastgraf/types/xy.obj out/fastgraf/types/rect.obj out/fastgraf/types/tri.obj out/fastgraf/types/fixedpt.obj out/fastgraf/types/fixedptc.obj \
				out/fastgraf/state.obj out/fastgraf/setters.obj out/fastgraf/pen.obj \
				out/sweet16.obj \
				out/wad/bundle.obj
	gsplnk link.cmd
	gsprom -i out\prog.out
	move /y src/*.lst out
	move /y *.lst out
	move /y *.asm out
	hex2bin out\prog.hi out\rc2014\proghi.bin
	hex2bin out\prog.lo out\rc2014\proglo.bin

out/%.obj: src/types/%.c
	$(CC_CMD)


out/palettes/%.obj: src/palettes/%.asm
	$(AS_CMD)

out/wad/%.obj: src/wad/%.asm
	$(AS_CMD)

out/fastgraf/prims/%.obj: src/fastgraf/prims/%.c
	$(CC_CMD)

out/fastgraf/prims/%.obj: src/fastgraf/prims/%.asm
	$(AS_CMD)

out/fastgraf/types/%.obj: src/fastgraf/types/%.asm
	$(AS_CMD)

out/textures/%.obj: src/textures/%.asm
	$(AS_CMD)

out/%.obj: src/%.asm
	$(AS_CMD)

out/%.obj: src/%.c
	$(CC_CMD)

out/fastgraf/%.obj: src/fastgraf/%.c
	$(CC_CMD)

out/fastgraf/%.obj: src/fastgraf/%.asm
	$(AS_CMD)

.PHONY: clean

clean:
	rm ./out/*.*
	rm ./out/fastgraf/*.*
	rm ./out/fastgraf/prims/*.*
	rm ./out/fastgraf/types/*.*