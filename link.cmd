/* Options */
-c
-o out\prog.out
-m out\prog.map
-stack 32768

/* INPUT FILES */
out\util\printf.obj
out\util\json.obj

out\sweet16.obj
out\prims.obj
out\isr.obj
out\hello.obj

out\loadpal.obj
out\framelp.obj

out\g_entry.obj
out\g_world.obj
out\i_inputs.obj
out\r_raycst.obj
out\r_txdata.obj
out\r_wall.obj
out\sys_uart.obj
out\v_video.obj
out\w_wadmgr.obj

out\wad\bundle.obj

out\fastgraf\prims\drawline.obj
out\fastgraf\prims\drawrect.obj
out\fastgraf\prims\drawfont.obj
out\fastgraf\prims\drawoval.obj
out\fastgraf\prims\drawtri.obj
out\fastgraf\prims\primline.obj
out\fastgraf\prims\primrect.obj
out\fastgraf\prims\primpixl.obj

out\fastgraf\types\rect.obj
out\fastgraf\types\xy.obj
out\fastgraf\types\tri.obj
out\fastgraf\types\fixedpt.obj
out\fastgraf\types\fixedptc.obj

out\fastgraf\pen.obj
out\fastgraf\state.obj
out\fastgraf\setters.obj

lib\c.lib

MEMORY
{
	RAM:			origin = 0x00800000, length = 0x3ffff0
	ROM (R):		origin = 0xfff00000, length = 0xffe00
	VECTOR_DI (R):	origin = 0xfffffea0, length = 0x20
	VECTOR_RST (R):	origin = 0xffffffe0, length = 0x20
}

SECTIONS
{
	.cinit:		{} > ROM
	.text: 		{} > ROM
	.data:		{} > ROM
	.bss:  		{} > RAM
	.stack:		{} > RAM
	.vectorrst: {} > VECTOR_RST
	.vectordi:  {} > VECTOR_DI
}